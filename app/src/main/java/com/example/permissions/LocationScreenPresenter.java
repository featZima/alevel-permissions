package com.example.permissions;

import androidx.annotation.Nullable;

public class LocationScreenPresenter {

    @Nullable
    private LocationScreenView view;

    public void setView(@Nullable LocationScreenView view) {
        this.view = view;
    }

    public void formatLocation(double lon, double lat) {
        String formatted = String.format("%f -- %f", lon, lat);
        if (view != null) {
            view.setLocation(formatted);
        }
    }
}
