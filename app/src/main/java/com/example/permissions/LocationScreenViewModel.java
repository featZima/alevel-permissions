package com.example.permissions;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

public class LocationScreenViewModel {

    public MutableLiveData<Coordinates> inputLocation;
    public LiveData<String> formattedLocation;

    public LocationScreenViewModel() {
        inputLocation = new MutableLiveData<>();
        formattedLocation = Transformations.map(inputLocation, new Function<Coordinates, String>() {
            @Override
            public String apply(Coordinates input) {
                return String.format("%f -- %f", input.getLat(), input.getLon());
            }
        });
    }
}
