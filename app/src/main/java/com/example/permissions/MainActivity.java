package com.example.permissions;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;

public class MainActivity extends AppCompatActivity implements LocationScreenView {

    private final static Integer REQUEST_AUDIO_PERMISSION = 101;
    private FloatingActionButton fab;
    private FusedLocationProviderClient fusedLocationClient;
    private GoogleApiClient client;
    private TextView presenterLocationTextView;
    private TextView viewModelLocationTextView;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationScreenPresenter presenter;
    private LocationScreenViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        presenterLocationTextView = findViewById(R.id.presenterLocationTextView);
        viewModelLocationTextView = findViewById(R.id.viewModelLocationTextView);

        presenter = new LocationScreenPresenter();
        presenter.setView(this);

        viewModel = new LocationScreenViewModel();
        viewModel.formattedLocation.observe(this, new Observer<String>() {
            @Override
            public void onChanged(String formattedLocation) {
                viewModelLocationTextView.setText(formattedLocation);
            }
        });

        client = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, null)
                .addApi(LocationServices.API)
                .build();
        client.connect();

        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String permission = Manifest.permission.ACCESS_FINE_LOCATION;
                if (ContextCompat.checkSelfPermission(MainActivity.this, permission)
                        == PackageManager.PERMISSION_GRANTED) {
                    // do action
                    doUsefulAction();
                } else {
                    // request permission
                    if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                            permission)) {
                        new AlertDialog.Builder(MainActivity.this)
                                .setTitle("Rationale message")
                                .setMessage("We really need the audio permission")
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        requestPermission(permission);
                                    }
                                })
                                .show();
                    } else {
                        requestPermission(permission);
                    }
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.setView(null);
    }

    private void requestPermission(String permission) {
        String[] permissions = new String[1];
        permissions[0] = permission;
        ActivityCompat.requestPermissions(MainActivity.this, permissions, REQUEST_AUDIO_PERMISSION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_AUDIO_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                doUsefulAction();
            }
        }
    }

    private LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            super.onLocationResult(locationResult);
            Location location = locationResult.getLastLocation();
            presenter.formatLocation(location.getLatitude(), location.getLongitude());
            viewModel.inputLocation.setValue(new Coordinates(location.getLatitude(), location.getLongitude()));
        }
    };

    @SuppressLint("MissingPermission")
    private void doUsefulAction() {
        LocationRequest mLocationRequest;
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(1000);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                locationCallback,
                null /* Looper */);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mFusedLocationClient.removeLocationUpdates(locationCallback);
    }

    @Override
    public void setLocation(String locationValue) {
        presenterLocationTextView.setText(locationValue);
    }
}
